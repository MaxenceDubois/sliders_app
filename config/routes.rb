Rails.application.routes.draw do
  resources :slides, only: [:index]
  namespace :admin do
    resources :slides
    root 'slides#index'
  end
  resources :slides
  root 'slides#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
