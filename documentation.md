# Documentation of mysliders

## Introduction

MySliders is an application in Ruby on Rails project for the creation of images slider on your website. The content is managed through an administrator account which is the only one that can add, edit or delete images. Visitors are only able to see the images.

## Requirements

Mysliders requires Ruby version >=2.1, Rails version >=5.0, Paperclip gem version >=5.0.0 and rails-admin-scaffold >=   .

## Getting start

### Installation of the application

Copy the following line while in your project directory :

rails new Mysliders --skip-bundle

Add Paperclip gem and rails-admin-scaffold gem in your Gemfile then copy :

bundle update

### Creation of the database

Copy the following line while in your project directory :

rails g scaffold slide name:string desc_left:text desc_right:text


### Routes.rb

resources :slides, only: [:index]
  namespace :admin do
     resources :slides
     root 'slides#index'
  end
  root 'slides#index'

### Database and migrations

Create a new migration for your database :

rails g migration AddPictureToSlide

Creation of the database :

rails g scaffold slide name:string desc_left:text desc_right:text


### Create the back office

Edit routes with the command :

rake routes

### Models

class User < ActiveRecord::Base
  has_attached_file :picture, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :picture, content_type: /\Aimage\/.*\z/
end

### Controller

Add the picture controller : 

def slide_params
      params.require(:slide).permit(:name, :desc, :author, :picture)
end

### Views

In admin and user, add pictures :

<p>
  <strong>Picture:</strong>
  <%= @slide.picture %>
</p>

Edit the index file in your view to add pictures :

<% @slides.each do |slide| %>
      <tr>
        <td><%= slide.name %></td>
        <td><%= slide.desc %></td>
        <td><%= slide.author %></td>
        <td><%= image_tag slide.picture.url(:thumb) %></td>
        <td><%= link_to 'Show', slide %></td>
      </tr>
<% end %>

For admin space :

<% @slides.each do |slide| %>
      <tr>
        <td><%= slide.name %></td>
        <td><%= slide.desc %></td>
        <td><%= slide.author %></td>
        <td><%= image_tag slide.picture.url(:thumb) %></td>
        <td><%= link_to 'Show', [:admin, slide] %></td>
        <td><%= link_to 'Edit', edit_admin_slide_path(slide) %></td>
        <td><%= link_to 'Destroy', [:admin, slide], method: :delete, data: { confirm: 'Are you sure?' } %></td>
      </tr>
<% end %>

<p>
  <strong>Picture:</strong>
  <%= image_tag @slide.picture.url %>
</p>

<% @slides.each do |slide| %>
   
        <%= slide.desc %>
      
        <%= slide.author %>
      
        <%= image_tag slide.picture.url %>
      
<% end %>

### Apply modifications

type folloying command to import modifications in application :

rake db:create
rake db:migrate

### Validations

See the Paperclip doc for available validation procedure for attachment.

## Bugs

To file a bug repport, please send an e-mail to example@mail.ext or file an issue on project's Gihub : https://github.com/XXXX/Mysliders.

Please include with your report: 
* the example input
* the output you expected
* the output slider actually produced

## Contributing

Pull requests for fixing bugs are welcome. Proposed new features are going to be meticulously reviewed -- taking into account backward compatibility, potential side effects, and future extensibility -- before deciding on acceptance or rejection.

